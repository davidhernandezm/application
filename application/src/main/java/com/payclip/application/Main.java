package com.payclip.application;

import com.payclip.application.controller.ClientController;
import com.payclip.application.exception.DataSourceAccessException;
import com.payclip.application.exception.JsonInformationReadingException;

/**
 * This class is in charge of taking the command line arguments and to call the controller methods,
 * depending on the provided transaction
 * 
 * @author David Herández
 * @version 1.0
 */
public class Main 
{
	public static void main( String[] args )
	{	
		//This method trigger input validations and it will communicate with the controller 
		handleTransaction(args);

	}
	
	/****************************************METHODS********************************************************/
	
	/**
	 * This method validates the entered information and calls the respective controller. 
	 * @param arguments Array received in the main() method.
	 */
	public static void handleTransaction(String[] arguments)   {

		ClientController clientController=new ClientController();
		ArgumentProcessor argumentProccessor=new ArgumentProcessor();

		try {
			if(argumentProccessor.validateArguments(arguments))
			{
				switch(argumentProccessor.getTransactionType())
				{
				case "add": 
					clientController.addTransaction(argumentProccessor.getUserId(), argumentProccessor.getClienTransactionDto());
					break;
				case "show": 
					clientController.showTransaction(argumentProccessor.getUserId(), argumentProccessor.getTransactionId());
					break;
				case "list": 
					clientController.showTransactions(argumentProccessor.getUserId());
					break;
				case "sum": 
					clientController.sumTransaction(argumentProccessor.getUserId());
					break;
				default: 
					printMessage("arguments-error");
					break;
				}
			}
			else
			{
				printMessage("arguments-error");
			}
		} catch (Exception e) {
			
			if(e instanceof JsonInformationReadingException )
			{
				printMessage("general-error");
				printMessage("transactionJson-error");
			}
				
			else
			{
				if(e instanceof DataSourceAccessException)
				{
					printMessage("dataSourceAcces-error");
				}
				else
					printMessage("general-error");	
			}
		}
	}
	/**
	 * This methods prints out the messages.
	 * 
	 * @param messageType is the type of message is going to be print out.
	 */
	public static void printMessage(String messageType) {

		if(messageType.equals("arguments-error"))
		{
			System.out.println("There was a problem processing the information entered by command-line.");
			System.out.println("Plese enter a valid transaction. See the following definitions:");
			System.out.println("");
			System.out.println("ADD TRANSACTION: ./application <user_id> add <transaction_json>");
			System.out.println("SHOW TRANSACTION: ./application <user_id> <transaction_id>");
			System.out.println("LIST TRANSACTIONS: ./application <user_id> list");
			System.out.println("SUM TRANSACTIONS: ./application <user_id> sum");
		}
		else
			if(messageType.equals("transactionJson-error"))
			{
				System.out.println("Please validate the attributes and data types in <transaction_json> object");
	
			}
			else
				if(messageType.equals("dataSourceAcces-error"))
				{
					System.out.println("There was a problem with the data source.");
		
				}
				else
					if(messageType.equals("general-error"))
					{
						System.out.println("There was a problem processing the transaction.");

					}
	}
}

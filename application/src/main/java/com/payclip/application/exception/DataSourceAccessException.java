package com.payclip.application.exception;

/**
 * This exception must be thrown for any problem that prevents to read or save from the data sources(data base, files, cloud etc). 
 * 
 * @author David Herández
 * @version 1.0
 */
public class DataSourceAccessException extends Exception {

	/**
	 * Serialization identifier.
	 */
	private static final long serialVersionUID = 1L;
	
	/****************************************CONSTRUCTORS********************************************************/
	public DataSourceAccessException(String message) {
		super(message);
	}
}
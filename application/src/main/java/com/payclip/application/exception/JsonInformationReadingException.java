package com.payclip.application.exception;

/**
 * This exception must be thrown for any problem with the json_transaction information provided by command line or for the information comming from the data source
 * that doesn't have all the parameters or data types or the correct sintaxis enogh to store or read successfuly an operation.   
 * 
 * @author David Hernández
 * @version 1.0
 */
public class JsonInformationReadingException extends Exception  {

	/**
	 * Serialization identifier.
	 */
	private static final long serialVersionUID = 1L;
	
	/****************************************CONSTRUCTORS********************************************************/
	public JsonInformationReadingException(String message) {
		super(message);
	}
}
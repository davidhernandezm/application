package com.payclip.application.controller;

import java.security.SecureRandom;
import java.util.List;
import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.payclip.application.exception.DataSourceAccessException;
import com.payclip.application.model.ClientTransactionDTO;
import com.payclip.application.model.FileSystemClientTransaction;

/**
 * After command line argument validations and depending the transaction types this class provide the 
 * required method to interact wuth the model layer.
 * 
 * @author David Herández
 * @version 1.0
 */
public class ClientController {
	
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();
	
	/****************************************CONSTRUCTORS********************************************************/
	public ClientController(){}
	
	/*********************************************METHODS********************************************************/
	
	/**
	 * This method is called for adding transaction.
	 * 
	 * @param userId user which is going to be stored the transaction
	 * @param clientTransactionInformation this object contains all the transaction information so far
	 * @throws Exception this is a general Exception for any problem.
	 * @throws DataSourceAccessException this exception will be thrown if exist any data source connection issue.  
	 */
	public void addTransaction(Integer userId,ClientTransactionDTO clientTransactionInformation) throws Exception,DataSourceAccessException {

		FileSystemClientTransaction clientTransactionFs= new FileSystemClientTransaction();

		ClientTransactionDTO clienTransactionDto=clientTransactionInformation;


		clienTransactionDto.setTransactionId(generateTransactionId(36));	
		/************Double check what number must be used****************/
		clienTransactionDto.setUserId(userId);

		clientTransactionFs.addTransaction(clienTransactionDto);
	}

	/**
	 * This method is called for showing an specific transaction.
	 * 
	 * @param userId searched user 
	 * @param transactionId searched transaction 
	 * @throws Exception this is a general Exception for any problem.
	 * @throws DataSourceAccessException this exception will be thrown if exist any data source connection issue.  
	 */
	public void showTransaction(Integer userId,String transactionId) throws Exception,DataSourceAccessException {
		FileSystemClientTransaction clientTransactionFs= new FileSystemClientTransaction();
		ClientTransactionDTO clientTransactionDto=clientTransactionFs.showTransaction(userId,transactionId);
		if(clientTransactionDto!=null)
			System.out.println(clientTransactionDto.toJsonString());
		else
			System.out.println("Transaction not found");
	}

	/**
	 * This method is called for listing all the transaction for an specific user.
	 * 
	 * @param userId searched user  
	 * @throws Exception this is a general Exception for any problem.
	 * @throws DataSourceAccessException this exception will be thrown if exist any data source connection issue.  
	 */
	public void showTransactions(Integer userId) throws Exception,DataSourceAccessException {
		FileSystemClientTransaction clientTransactionFs= new FileSystemClientTransaction();
		List<ClientTransactionDTO> transactionList=clientTransactionFs.showTransactions(userId);
		System.out.print((transactionList.size()>0)?"[\n":"[");
		for(ClientTransactionDTO transaction:transactionList)
		{
			System.out.println(transaction.toJsonString());
		}
		System.out.println("]");
	}

	/**
	 * This method is called for counting all the transaction for an specific user.
	 * 
	 * @param searched userId
	 * @throws Exception this is a general Exception for any problem.
	 * @throws DataSourceAccessException this exception will be thrown if exist any data source connection issue.  
	 */
	public void sumTransaction(Integer userId) throws Exception,DataSourceAccessException {

		JsonObjectBuilder sumUserTrasaction;
		FileSystemClientTransaction clientTransactionFs= new FileSystemClientTransaction();
		sumUserTrasaction = Json.createObjectBuilder();
		sumUserTrasaction.add("user_id", userId);
		sumUserTrasaction.add("sum", clientTransactionFs.sumTransaction(userId));

		System.out.println(sumUserTrasaction.build().toString());

	}

	/**
	 * This method generates random alphanumeric numbers.
	 * 
	 * @param length This is the length the alphanumeric will have. 
	 * @return
	 */
	String generateTransactionId( int length ){
		StringBuilder sb = new StringBuilder( length );
		for( int i = 0; i < length; i++ ) 
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		return (sb.toString().substring(0, 8)
				+"-"+sb.toString().substring(9, 13)
				+"-"+sb.toString().substring(14, 18)
				+"-"+sb.toString().substring(19,23 )
				+"-"+sb.toString().substring(24, 36)).toLowerCase();
	}


}

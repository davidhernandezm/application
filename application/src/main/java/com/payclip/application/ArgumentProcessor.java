package com.payclip.application;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import com.payclip.application.exception.JsonInformationReadingException;
import com.payclip.application.model.ClientTransactionDTO;

/**
 * This class is in charge of validating the command line argumentes provided by the final user. 
 * 
 * @author David Herández
 * @version 1.0
 */
public class ArgumentProcessor {

	HashMap<String, List<String>> availableOperations;
	Integer userId;
	String transactionType;
	String transactionId;
	ClientTransactionDTO clienTransactionDto;
	
	/****************************************CONSTRUCTORS********************************************************/
	
	/**
	 * Constructor for loading the available transaction.
	 */
	public ArgumentProcessor() {

		this.availableOperations = new HashMap<String, List<String>>();

		availableOperations.put("add",Arrays.asList("userId","transactionType", "transactionJson"));
		availableOperations.put("show",Arrays.asList("userId", "transactionId", "transactionType"));
		availableOperations.put("list",Arrays.asList("userId","transactionType"));
		availableOperations.put("sum",Arrays.asList("userId","transactionType"));


	}
	
	/*********************************************METHODS********************************************************/
	
	/**
	 * @param arguments[] String array with the information provided by command line. 
	 * @return boolean if true the arguments were validated successfuly
	 * @throws JsonInformationReadingException This exception will be throwed if  information in the  transaction_json is incorrect or incomplete
	 */
	public boolean validateArguments(String[] arguments) throws JsonInformationReadingException{
		List<String> operation=null;
		try {
			//We validate there are more than two parameter as minimum 
			if(arguments.length>1)
			{
	
				operation = availableOperations.get(arguments[1].toLowerCase());

				if(operation!=null)		
					for(String attribute:operation )
					{
						if(attribute=="userId")
							this.userId=Integer.parseInt(arguments[0]);
						else
						{
							if(attribute=="transactionJson")
							{
								//If the conversions are successful we have valid-arguments
								String jsonString ="";
								String subArray[]=Arrays.copyOfRange(arguments, 2,(arguments.length));
								//we add the space we have remove to fill args[] out for main method
								//it is helpful for description attribute
								for(String string:subArray)
								{
									jsonString=jsonString.concat(string+" ");
								}
								String formatedJson=jsonString.replaceAll("“", "\"").replaceAll("”","\"");
								try {
								JsonReader reader = Json.createReader(new StringReader( formatedJson));
								JsonObject jsonTransactionDetail = reader.readObject();	
								this.clienTransactionDto = new ClientTransactionDTO();
								clienTransactionDto.preparedAddTransactionValues(jsonTransactionDetail);
								}
								catch (Exception e) {
									throw new JsonInformationReadingException("Please validate the attributes and data types in <transaction_json> object");
								}

							}
							else
								if(attribute=="transactionType")
								{
									this.transactionType=arguments[1];
								}


						}
					}
				//So, the parameters entered by command line do not have an explicit transactionType
				//So far, it's the only wrote definition we have.
				if(operation==null)
				{
					for(String attribute:availableOperations.get("show") )
					{
						if(attribute=="userId")
							this.userId=Integer.parseInt(arguments[0]);
						else
						{
							if(attribute=="transactionId")
							{
								this.transactionId=arguments[1];
							}
							else
								if(attribute=="transactionType")
								{
									this.transactionType="show";
								}
						}
					}
					return true;
				}
				else 
				{
					return true; //Operation was found, and the values were casted successfully
				}
			}
			else
			{	
				return false; //Invalid-less than 2 arguments
			}
		}//End Try block

		catch (Exception e) {
			if(e instanceof JsonInformationReadingException)
				throw new JsonInformationReadingException("Please validate the attributes and data types in <transaction_json> object");
			//e.printStackTrace();
			//Any problem with conversions or any access problem  with args-array means 
			//the information provided by command line is incorrect
			return false;
		}

	}
	
	/****************************************GETTERS & SETTERS********************************************************/
	
	/**
	 * 
	 * @return the clienTransactionDto
	 */
	public ClientTransactionDTO getClienTransactionDto() {
		return clienTransactionDto;
	}

	/**
	 * @param clienTransactionDto the clienTransactionDto to set
	 */
	public void setClienTransactionDto(ClientTransactionDTO clienTransactionDto) {
		this.clienTransactionDto = clienTransactionDto;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
}

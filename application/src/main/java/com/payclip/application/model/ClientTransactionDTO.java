package com.payclip.application.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 * This class defines the required fields and information data types  that will be read and wrote by the model layers.
 * This is a data transfer object to be used by all the layers through the application. 
 * 
 * @author David Hernández
 * @version 1.0
 */
public class ClientTransactionDTO {

	private Integer userId;
	private String transactionId;
	private float amount;
	private String description;
	private Date date; 
	
	/****************************************CONSTRUCTORS********************************************************/
	public ClientTransactionDTO() {}
	
	public ClientTransactionDTO(JsonObject jsonTransactionDetail) throws ParseException  {

		if(jsonTransactionDetail.containsKey("transaction_id"))
		{
			this.transactionId=jsonTransactionDetail.getString("transaction_id");
		}
		if(jsonTransactionDetail.containsKey("amount"))
		{
			this.amount=Float.parseFloat(jsonTransactionDetail.get("amount").toString());
		}
		if(jsonTransactionDetail.containsKey("description"))
		{
			this.description=jsonTransactionDetail.getString("description");
		}
		if(jsonTransactionDetail.containsKey("date"))
		{
			this.date= new java.sql.Date(new SimpleDateFormat("yyyy-mm-dd").parse(jsonTransactionDetail.getString("date")).getTime());

		}
		if(jsonTransactionDetail.containsKey("user_Id"))
		{
			this.userId=Integer.parseInt(jsonTransactionDetail.getString("user_Id"));
		}
	}
	
	/*********************************************METHODS********************************************************/
	
	/**
	 * This method validate all the required information for an ADD transaction.
	 * 
	 * @param jsonTransactionDetail A Json object with the information provided by command line (json_transacion argument)
	 * @throws ParseException this exception will be thrown If there is any cast problem.
	 */
	public void preparedAddTransactionValues(JsonObject jsonTransactionDetail) throws ParseException  {
	
		this.amount=Float.parseFloat(jsonTransactionDetail.get("amount").toString());
		this.description=jsonTransactionDetail.getString("description");
		this.date= new java.sql.Date(new SimpleDateFormat("yyyy-mm-dd").parse(jsonTransactionDetail.getString("date")).getTime());
		this.userId=Integer.parseInt(jsonTransactionDetail.get("user_id").toString());
						
	}
		
	/**
	 * Method to print out a string with all the attribute values of this object in a json format.
	 * 
	 * @return string with the attribute values of the object. 
	 */
	public String toJsonString() {
		String formatedDate = new SimpleDateFormat("yyyy-mm-dd")
				.format(this.date);
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder()
				.add("transaction_id", this.transactionId)
				.add("amount", Double.parseDouble(Float.toString(this.amount)))
				.add("description", this.description)
				.add("date", formatedDate)
				.add("user_id", this.userId);

		return objectBuilder.build().toString();
	}
	
	/********************************************GETTERS & SETTERS************************************************************/

	/**
	 * @return
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return
	 */
	public float getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 */
	public void setAmount(float amount) {	
		this.amount =amount;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 */
	public void setDate(Date date) {
		this.date = date;
	}
}

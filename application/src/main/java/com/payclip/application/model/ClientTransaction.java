package com.payclip.application.model;

import java.util.List;

/**
 * This interface define methods,  method arguments, returns and required data types that must be used 
 *  when a new data source is added.
 * 
 * @author David Hernández
 * @version 1.0
 *
 */
public interface ClientTransaction {

	/**
	 * This method is called for adding transaction.
	 * 
	 * @param clientTransactionDto This object contains all the required information when a new transaction is going to be added.
	 * @return Return a boolean value if the transaction was  completed successfully or not.
	 * @throws Exception The exceptions will be managed in higher layers, for any problem with the data source
	 * that prevents read or save a transaction a DataSourceAccessException exception type must be thrown.  
	 */
	public boolean addTransaction(ClientTransactionDTO clientTransactionDto) throws Exception;

	/**
	 * This method is called for showing an specific transaction.
	 * 
	 * @param userId This is the user is going to be searched.
	 * @param transactionId This is the specific searched transaction. 
	 * @return This method returns a ClientTransactionDTO object with all the transaction information.
	 * 			If was not found the transaction, null must be returned. 
	 * @throws Exception The exceptions will be managed in higher layers, for any problem with the data source
	 * that prevents read or save a transaction a DataSourceAccessException exception type must be thrown.
	 */
	public ClientTransactionDTO showTransaction(Integer user_Id, String transactionId) throws Exception;

	/**
	 * This method is called for listing all the transaction for an specific user.
	 * 
	 * @param userId This is the user is going to be searched.
	 * @return This method returns a ClientTransactionDTO list for all the found transactions for that userId. 
	 * 			If there is not any transaction recorded for that user a ClientTransactionDTO empty list must be returned.
	 * @throws Exception The exceptions will be managed in higher layers, for any problem with the data source
	 * that prevents read or save a transaction a DataSourceAccessException exception type must be thrown.
	 */
	public List<ClientTransactionDTO> showTransactions(Integer user_Id) throws Exception;

	/**
	 * This method is called for counting all the transaction for an specific user.
	 * 
	 * @param userId This is the user is going to be searched.
	 * @return int This method returns how many transactions there is stored for that user.
	 * 		   If there is not transactions for that user the method must return a number 0. 	
	 * @throws Exception The exceptions will be managed in higher layers, for any problem with the data source
	 * that prevents read or save a transaction a DataSourceAccessException exception type must be thrown.
	 */
	public int sumTransaction (Integer user_Id) throws Exception;

}

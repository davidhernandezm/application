package com.payclip.application.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;

import com.payclip.application.exception.DataSourceAccessException;

/**
 * This class implements all the methods provided by ClientTransaction interface using a File System Storage approach.
 * 
 * @author David Hernández
 * @version 1.0
 */
public class FileSystemClientTransaction implements ClientTransaction {
	
	Comparator<ClientTransactionDTO> transactionDateComparator = (c1, c2) -> new SimpleDateFormat("yyyy-mm-dd").format(c1.getDate()).compareTo(new SimpleDateFormat("yyyy-mm-dd").format(c2.getDate())); 
	
	/******************************************CONSTRUCTORS*********************************************************/
	public FileSystemClientTransaction(){}
	
	/********************************************METHODS************************************************************/
	
	/**
	 * This method is called for adding transaction.
	 * 
	 * @param clientTransactionDto This object contains all the required information when a new transaction is going to be added.
	 * @return Return a boolean value if the transaction was  completed successfully or not.
	 * @throws Exception The exceptions will be managed in higher layers, for any problem with the data source
	 * that prevents read or save a transaction a DataSourceAccessException exception type must be thrown.  
	 */
	public boolean addTransaction(ClientTransactionDTO clientTransactionDto) throws Exception {
		boolean transactionApplied=false;	
		BufferedWriter bufferedWriter=null;
		try
		{

			FileWriter writer = new FileWriter("./client_transaction.txt", true);
			bufferedWriter = new BufferedWriter(writer);
			String transactionLine=clientTransactionDto.getTransactionId()+"|"+
					clientTransactionDto.getAmount()+"|"+
					clientTransactionDto.getDescription()+"|"+
					new SimpleDateFormat("yyyy-mm-dd").format(clientTransactionDto.getDate())+"|"+
					clientTransactionDto.getUserId();
			bufferedWriter.write(transactionLine);
			bufferedWriter.newLine();

			System.out.println(clientTransactionDto.toJsonString());
			transactionApplied=true; 
		}

		catch(Exception e)
		{
				
			if(e instanceof IOException||e instanceof FileNotFoundException)	
				throw new DataSourceAccessException("There was a problem with the data source.");
			else
				throw new Exception("There was a problem processing the transaction");
		}
		finally {
			try {
				if(bufferedWriter!=null)
					bufferedWriter.close();
			} catch (IOException e) {
				
				throw new DataSourceAccessException("There was a problem with the data source.");
			}
		}
		return transactionApplied;

	}
	
	/**
	 * This method is called for showing an specific transaction.
	 * 
	 * @param userId This is the user is going to be searched.
	 * @param transactionId This is the specific searched transaction. 
	 * @return This method returns a ClientTransactionDTO object with all the transaction information.
	 * 			If was not found the transaction, null must be returned. 
	 * @throws Exception The exceptions will be managed in higher layers, for any problem with the data source
	 * that prevents read or save a transaction a DataSourceAccessException exception type must be thrown.
	 */
	public ClientTransactionDTO showTransaction(Integer userId, String transactionId) throws Exception{
		ClientTransactionDTO clientTransactionDTO=null;
		BufferedReader bufferedReader =null;
		try { 
			bufferedReader = new BufferedReader(new FileReader("./client_transaction.txt")); 
			for(String line; (line = bufferedReader.readLine()) != null; ) {
				String transDetailTmp[]=line.split("\\|");
				if(transDetailTmp.length>4&&transDetailTmp[0].equals(transactionId)&&transDetailTmp[4].equals(Integer.toString((userId))))
				{
					clientTransactionDTO= new ClientTransactionDTO();
					clientTransactionDTO.setTransactionId(transDetailTmp[0]);
					clientTransactionDTO.setAmount(Float.parseFloat(transDetailTmp[1]));
					clientTransactionDTO.setDescription(transDetailTmp[2]);
					try {
						clientTransactionDTO.setDate(new Date(new SimpleDateFormat("yyyy-mm-dd").parse(transDetailTmp[3]).getTime()));
						
					} catch (ParseException e) {
						throw new Exception("There was a problem processing the transaction");
					}
					clientTransactionDTO.setUserId(Integer.parseInt(transDetailTmp[4]));

					break;
				}		
			}		
		}
		catch(Exception e)
		{
				
			if(e instanceof IOException||e instanceof FileNotFoundException)		
				throw new DataSourceAccessException("There was a problem with the data source.");
			else
				throw new Exception("There was a problem processing the transaction");
		}
		finally {
			try {
				if(bufferedReader!=null)
				bufferedReader.close();
			} catch (IOException e) {
				
				throw new DataSourceAccessException("There was a problem with the data source.");
			}
		}
		return clientTransactionDTO;
	}
	
	/**
	 * This method is called for listing all the transaction for an specific user.
	 * 
	 * @param userId This is the user is going to be searched.
	 * @return This method returns a ClientTransactionDTO list for all the found transactions for that userId. 
	 * 			If there is not any transaction recorded for that user a ClientTransactionDTO empty list must be returned.
	 * @throws Exception The exceptions will be managed in higher layers, for any problem with the data source
	 * that prevents read or save a transaction a DataSourceAccessException exception type must be thrown.
	 */
	public List<ClientTransactionDTO> showTransactions(Integer userId)  throws Exception{
		List<ClientTransactionDTO> clientTransactionDTOList = new ArrayList<ClientTransactionDTO>();
		BufferedReader bufferedReader =null;
		try {
			bufferedReader = new BufferedReader(new FileReader("./client_transaction.txt"));
			for(String line; (line = bufferedReader.readLine()) != null; ) {
				String transDetailTmp[]=line.split("\\|");	
				if(transDetailTmp.length>4&&transDetailTmp[4].equals(Integer.toString(userId))) {
					ClientTransactionDTO clientTransactionDTO= new ClientTransactionDTO();
					clientTransactionDTO.setTransactionId(transDetailTmp[0]);
					clientTransactionDTO.setAmount(Float.parseFloat(transDetailTmp[1]));
					clientTransactionDTO.setDescription(transDetailTmp[2]);
					try {
						clientTransactionDTO.setDate(new Date(new SimpleDateFormat("yyyy-mm-dd").parse(transDetailTmp[3]).getTime()));
						 
					
					} catch (ParseException e) {
						throw new Exception("There was a problem processing the transaction");
					}
					clientTransactionDTO.setUserId(Integer.parseInt(transDetailTmp[4]));
					clientTransactionDTOList.add(clientTransactionDTO);
					clientTransactionDTOList.sort(transactionDateComparator.reversed());
				}	
			}
		}
		catch(Exception e)
		{
				
			if(e instanceof IOException||e instanceof FileNotFoundException)		
				throw new DataSourceAccessException("There was a problem with the data source.");
			else
				throw new Exception("There was a problem processing the transaction");		}
		finally {
			try {
				if(bufferedReader!=null)
				bufferedReader.close();
			} catch (IOException e) {
				
				throw new DataSourceAccessException("There was a problem with the data source.");
			}
		}
		
		return clientTransactionDTOList;
	}

	/**
	 * This method is called for counting all the transaction for an specific user.
	 * 
	 * @param userId This is the user is going to be searched.
	 * @return int This method returns how many transactions there is stored for that user.
	 * 		   If there is not transactions for that user the method must return a number 0. 	
	 * @throws Exception The exceptions will be managed in higher layers, for any problem with the data source
	 * that prevents read or save a transaction a DataSourceAccessException exception type must be thrown.
	 */
	public int sumTransaction(Integer userId) throws Exception{
		int sum=0;
		BufferedReader bufferedReader =null;
		try {
			bufferedReader = new BufferedReader(new FileReader("./client_transaction.txt")); 
			for(String line; (line = bufferedReader.readLine()) != null; ) {
				String transDetailTmp[]=line.split("\\|");
				if(transDetailTmp.length>4&&transDetailTmp[4].equals(Integer.toString(userId)))
					sum++;
			}
		}
		catch(Exception e)
		{
				
			if(e instanceof IOException||e instanceof FileNotFoundException)	
				throw new DataSourceAccessException("There was a problem with the data source.");
			else
				throw new Exception("There was a problem processing the transaction");
		}
		finally {
			try {
				if(bufferedReader!=null)
				bufferedReader.close();
			} catch (IOException e) {
				
				throw new DataSourceAccessException("There was a problem with the data source.");
			}
		}
		return sum;
	}

}
